---
layout: job_page
title: "Director of Developer Relations"
---

As the Director of Developer Relations, you will be responsible for managing and building the team that focuses on supporting the GitLab community. You will help to make it easy to contribute to GitLab, help our community give feedback to our product and documentation, and help to create educational programs for GitLab.

## Responsibilities  

* Build a global team to focus on the the developer experience of the GitLab community.  
* Lead the developer advocacy team.
* Build and execute a plan for the growth of the team.  
* Lead the technical writing team as they focus on making the GitLab documentation and developer content machine scalable and educational.  
* Define and execute a plan for community meetups.  
* Work with the technical writing team and content marketing to plan and help create engaging developer content for our blog, video, social media, and other outlets.  
* Work with our content marketing manager and technical writing team to define a scalable tutorial and quickstart guide plan. Launch this plan.  
* Work with our product marketing lead to support our monthly release with developer education tools for the new features and products launching.  
* Focus on developer experience and how developer relations can help get user feedback into the product and documentation.  
* Work with the field marketing manager to support events efforts through developer advocacy.  

## Requirements  

* You were a full-time developer in a previous life but prefer to work with the developer community to improve experience and support through education.   
* Be creative. You’ve made people happy with your quirky campaigns.  
* You give a great keynote and have videos to prove it.  
* You’ve led and built a team before.  
* Excellent spoken and written English.  
* Familiar with Git, Ruby, and GitLab.
* Analytical and data driven approach
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.  
* You are obsessed with making developers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.  
* You share our values, and work in accordance with those values.   

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
