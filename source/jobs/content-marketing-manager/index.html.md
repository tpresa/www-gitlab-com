---
layout: job_page
title: "Manager, Content Marketing"
---

GitLab is looking for a prolific content strategist and creator to join our team. This is a highly visible and valuable position to define GitLab’s content strategy and create content that will expand our company’s digital footprint, awareness, subscribers, and leads. Our ideal candidate is a strong writer and editor, is equally creative and detailed oriented.

## Responsibilities

- Manage a small team of content and social media marketers. 
- Define, implement, and regularly iterate on GitLab's content marketing strategy.
- Drive broad awareness of GitLab through thought leadership and PR efforts. 
- Develop and implement a content operations process for managing content development, publishing, amplification, and measurement. 
- Build and maintain a rich editorial calendar to support marketing campaigns, SEO, events, product releases, and company announcements. 
- Collaborate with product marketing to communicate our positioning and messaging to new audiences. 
- Grow and maintain our newsletter and webcast subscriber lists. 
- Oversee our webcast, blog, and social media programs. 
- Collaborate with sales team, advocates, influencers, industry experts to generate captivating use cases and thought leadership content. 
- Perform regular content and gap analysis to analyze which content and approaches are working and why.

## Requirements

- Degree in marketing, journalism, communications or related field
- 3-5 years experience in content marketing, social media, or journalism at a technology company, preferably an enterprise technology company
- A dual-minded approach: Highly creative and an excellent writer/editor but can also be process-driven, think scale, and rely on data to make decisions.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
- Extremely detail-oriented and organized, able to meet deadlines
- Obsessive about content quality not quantity
- Regular reporting on how content and channel performance to help optimize our content marketing efforts
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.

**NOTE** In the compensation calculator below, fill in "Lead" in the `Level` field for this role.

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
