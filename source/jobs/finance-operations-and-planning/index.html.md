---
layout: job_page
title: "Finance Operations and Planning"
---

You will be working directly with the CFO and executive team leaders across the company to enhance and improve our forecasting and business modeling capabilities.  You will also be working with our data and analytics team to help develop predictive modeling and decision support capabilities.

For this role, you should hold a degree in Finance or Accounting followed by relevant work experience in the software/SaaS industry. Knowledge of forecasting models and cost accounting processes are key requirements for this position. You will explore investment options and set company-wide financial policies.

Ultimately, you will ensure our financial planning is healthy and profitable and aligns with business objectives.

## Responsibilities

- Improve and enhance the corporate financial model to produce highly accurate monthly forecast of income statement, balance sheet and cash flows.
- Implement company wide headcount forecasting and requisition process.
- Deepen forecasting capability for revenue through predictive analysis based on company specific marketing and sales factors.
- Prepare cost projections
- Analyze and report on current financial status
- Conduct thorough research of historical financial data including detailed benchmarking analysis against industry comparables.
- Explore investment options and present risk and opportunities
- Coordinate with the CFO and the executive team on long-term financial planning
- Compare anticipated and actual results and identify areas of improvement
- Coordinate quarterly forecasting process working with executive team members and direct reports.
- Review accounting transactions for data accuracy
- Establish financial policies and document them in the GitLab handbook.
- Prepare visualization of financial data to promote internal and external understanding of the company’s financial results.

## Requirements

- Proven work experience as a Senior Financial Analyst, Financial Analyst or similar role
- Four years of experience with enterprise software of SaaS business models.
- Curiosity and the desire to continually grow and improve.
- A passion for data and for building scalable and sustainable systems.
- Hands-on experience with financial and visualization software
- Expertise in Google sheets (we do not use excel for modeling purposes)
- Consistent track record of using quantitative analysis to impact key business decisions.
- Excellent analytical skills
- Ability to present financial data using detailed reports and charts
- Demonstrable strategic thinking skills
- Confidentiality in handling sensitive financial information
- BS degree in Finance, Accounting or Economics.  Additional work in mathematics or statistics would be helpful.
- 3-5 years of experience building/automating/exploring analyses in any of SQL, R, Python, or highly scalable excel/VBA.
- Relevant certification (e.g. CFA/CPA) is a plus

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Director, Data & Analytics
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Next, candidates will be invited to schedule a 45 minute interview with our CCO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
