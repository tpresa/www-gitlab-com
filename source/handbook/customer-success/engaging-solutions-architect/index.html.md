---
layout: markdown_page
title: "Solutions Architects"
---

# Solutions Architect Handbook
{:.no_toc}

Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements. Solution Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process. Solution Architects are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the the technical solution while also understanding the business challenges the customer is trying to overcome.

## On this page
{:.no_toc}

- TOC
{:toc}

## When and How to Engage a Solutions Architect
Engaging GitLab Solutions Architects


### Before getting SA involved:

#### 1) Qualification:
* What is the qualified reason to engage with us?
* What is the size/quality/state of the opportunity?
* Have you created your plan?

#### 2) Preparation:

Help us, help them.  3 ASKs:
* Outcome.
* Infrastructure.
* Challenges.

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges (see below) information uncovered by the AM in prior interactions with the account.  Occasionally, we could support a combination call of discovery and technical demonstration/deep-dive, but this is suboptimal.  However, the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

**Outcome**
WIIFM/T.  What’s in it for them?  Why are they looking at a new strategy for s/w dev.  We need to be able to tailor our demos and presentations to demonstrate value and how we can address their issues.

**Infrastructure**
What i2p tools do they currently have in place?  E.g. Jenkins for CI, Ansible/Chef for automation, Codebear for review, VS/eclipse/emacs/vim for dev, etc.

**Challenges**
What problems/roadblocks are they having?  Release velocity, visibility, collaboration, automation.


## Engagement Protocol:

First, Navigate to the SA Service Desk Board [here](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477)

> NOTE: The board is located in the Group “Customer Success” and the name of the project is “SA Service Desk”

1) Create a [new issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)  
2) Use one of two templates...

*  If its the first engagement use the "SA Activity" template to ensure the proper data is collected about your needs.
*  If this is a follow up meeting, use the "Follow Up" template.

3) Add the “SA Backlog” label to the Issue  
4) The SA Team will triage the SA Backlog queue and collaborate with the submitter via Issue Discussions.

> NOTE: This will not replace SFDC.  The SAs will still be required to input the necessary information for each account that is critical to the strategy

* [Video: How to engage Solutions Architects using GitLab](https://drive.google.com/file/d/0BztS4JxtaDlrTnZIcWFhb0dWZ2c/view?ts=59879f01) Internal Only

### Labels
There are a number of labels the team uses to organize the SA Service Desk.  While you don't **need** to add these labels, you can help the [SA Triage Process]() by adding labels where appriate.

* **SA Backlog**: This label is automatically added by the `SA Activity` template.  Every issue with this label is addressed at every [SA triage call]()
* **Current Quarter Label**: This is a special label for this quarter that you can use to help us focus on deals that might should close this quarter.
* **EXPEDITE**: If the customer's need for a call is within 48 hours, add this label to ensure whoever is [running SA triage that week]() is alerted to the issue.
* **SA Waiting**: This label is added by the SA group when we are waiting on information from the account executive.  Once you provide this information, you should feel free to remove this label and make sure the issue is still labed with `SA Backlog` so that it is addressed at the next triage at the latest
