---
layout: markdown_page
title: "GitLab Inc (China) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to China based employees

The employer pays the social insurance and housing fund to the employees in China. The social insurance includes the pension, medical insurance, parturition insurance, work-related injury insurance, and unemployment insurance in accordance with the [scheme](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPSFluNzZscTFaMzQ/view?usp=sharing) chosen by GitLab.
